import React from "react";
import {useDispatch,useSelector} from "react-redux";
import {changeFormR} from "../state/user/users.actions";

const TestComponent =() => {
    // dispatch is the way we can manipulate our actions
    const dispatch = useDispatch()
    // selector we can select one property of the combineReducers
    const state = useSelector((store: any) => store.user.user)
    const handleClick = () => {
        dispatch(changeFormR())
    }
    return(
        <div>
            <h2> State {state ? <p>true</p> : <p>FALSE</p>}</h2>
            <button onClick={handleClick}>Change State</button>
        </div>
    )
}
export default TestComponent