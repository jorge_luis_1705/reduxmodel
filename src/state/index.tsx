import{combineReducers} from 'redux';
// import reducers to be used globally
import userReducer from "./user/users.reducer";
export default combineReducers({
    //  combineReducers is a function who recives an obj
    // that obj is all reducers   to be used globally
    user: userReducer
})