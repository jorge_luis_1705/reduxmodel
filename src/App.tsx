import React from 'react';
// state
import {Provider} from "react-redux";
import store from "./state/stores";
import TestComponent from "./components/testComponent";
function App() {
  return (
      // provider helps us to have our status globally
      // meanwhile store  is what all our reducers store
  <Provider store={store}>
    <div className="App">
      <TestComponent/>
    </div>
  </Provider>
  );
}

export default App;
